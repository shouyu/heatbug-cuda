#pragma comment (lib,"cudart.lib")

#ifdef _DEBUG
#pragma comment (lib,"cutil32D.lib")
#else
#pragma comment (lib,"cutil32.lib")
#endif

#include "HeatbugObserver.h"

int main(int argc, char** argv) {
	{
	HeatbugObserver heatbugObs(
		1024,	// window width
		1024,	// window height
		1024,	// space width
		1024,	// space height
		10000		// num of bugs
		);
	heatbugObs.buildObjects();
	//heatbugObs.cleanup();
	}

	return 0;
}