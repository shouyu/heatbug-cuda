#pragma once

struct C3FV3F {
	float r, g, b;
	float x, y, z;
	C3FV3F(float r=0, float g=0, float b=0, float x=0, float y=0, float z=0){
		this->r=r; this->g=g; this->b=b;
		this->x=x; this->y=y; this->z=z;
	}
};

struct C3FV3F4 {
	C3FV3F d[4];
};