#pragma once

#include "curand_kernel.h"
#include "Space.hpp"

struct Cell {
	int x, y;

	__host__ __device__ Cell() : x(0), y(0) {}
	__host__ __device__ Cell(int x_, int y_) : x(x_), y(y_) {}
};

class Heatbug
{
public:
	curandState_t state;

	double unhapiness;
	int x, y;
	int idealTemperature;
	int outputHeat;
	double randomMoveProbability;
	int id; // start from 1
	Cell target;

	__device__ Heatbug(void) { }

	__device__ ~Heatbug(void) { }

	__device__ void initRNG(int seed) {
		curand_init(seed, id, 0, &state);
	}

	__device__ void setOutputHeat(int minHeat, int maxHeat) {
		outputHeat = (int)(curand_uniform(&state) * (maxHeat-minHeat)) + minHeat;
	}

	__device__ void setIdealTemperature(int minTemp, int maxTemp) {
		idealTemperature = (int)(curand_uniform(&state) * (maxTemp-minTemp)) + minTemp;
	}

	__device__ void setRandomMoveProbability(double prob) {
		randomMoveProbability = prob;
	}

	__device__ void setPosition(int posX, int posY) {
		x = posX;
		y = posY;
	}

	__device__ void setRandomPos(int maxX, int maxY) {
		x = curand(&state) % maxX;
		y = curand(&state) % maxY;
	}

	__device__ void setId(int id) {
		this->id = id;
	}

	template<class T>
	__device__ Cell findBestPos(HeatSpace<T>* heat) {
		int _x, _y;
		int px, py;
		T best;
		Cell bestpos;

		best = heat->data[x + y * heat->width];
		int findhot = best < idealTemperature;
		//if(id == 1) printf("idealtemp: %d\n", idealTemperature);
		//if(id == 1) printf("curr: %d\n", best);

		for(_x = x - 1; _x <= x + 1; _x++) {
		for(_y = y - 1; _y <= y + 1; _y++) {
			if(_x == -1) px = heat->width-1;
			else if(_x >= heat->width) px = 0;
			else px = _x;
			if(_y == -1) py = heat->height-1;
			else if(_y >= heat->height) py = 0;
			else py = _y;
			//if(id == 1) printf("    check pos x:%d y:%d val:%d\n", px, py, heat->data[px + py * heat->width]);
			if(findhot) {
				
				if(best <= heat->data[px + py * heat->width]) {
					best = heat->data[px + py * heat->width];
					//if(id == 1) printf("  hotter pos x:%d, y:%d, val:%d\n", px, py, best);
					bestpos.x = px;
					bestpos.y = py;
				}
			} else {
				if(best >= heat->data[px + py * heat->width]) {
					best = heat->data[px + py * heat->width];
					//if(id == 1) printf("  cooler pos x:%d, y:%d, val:%d\n", px, py, best);
					bestpos.x = px;
					bestpos.y = py;
				}
			}
		}
		}

		return bestpos;
	}

	template<class T>
	__device__ void settarget(HeatSpace<T>* heat) {
		unhapiness = (double)(fabsf(idealTemperature - heat->data[x + y * heat->width]) / heat->maxHeat);
		//if(id == 1) printf("unhapiness: %f\n", unhapiness);
		if(curand_uniform(&state) < randomMoveProbability) {
			//if(id == 1) printf("random move\n", unhapiness);
			target.x = x + curand(&state) % 3 - 1;
			target.y = y + curand(&state) % 3 - 1;
			target.x = (target.x + heat->width) % heat->width;
			target.y = (target.y + heat->height) % heat->height;
		} else {
			//if(id == 1) printf("find best pos\n");
			if(unhapiness > 0) {
				target = findBestPos(heat);
				//if(id == 1) printf("best pos %d, %d\n", target.x, target.y);
				//if(id == 1) printf("curr pos %d, %d\n", x, y);
			} else {
				target = Cell(x, y);
			}
		}
	}

	template<class T>
	__device__ void move(HeatSpace<T>* heat, Grid2d<int>* world) {
		if(target.x == x && target.y == y) {
			heat->addHeat(outputHeat, x, y);
		} else {
			int tries = 0;
			while(tries < 9) {
				tries++;
				int targetIdx = target.x + target.y * heat->width;
				//if(id == 1) printf("target  %d, %d\n", target.x, target.y);
				//if(id == 1) printf("isempty %d\n", world->data[targetIdx]);
				if(atomicCAS(&(world->data[targetIdx]), 0, id) == 0) {
					atomicExch(&(world->data[x + y * heat->width]), 0);
					//printf("Moved Id:%d, X:%d,Y:%d->X:%d, Y:%d\n", id, x, y, target.x, target.y);
					break;
				}
				target.x = x + curand(&state) % 3 - 1;
				target.y = y + curand(&state) % 3 - 1;
				if(target.x == -1) target.x = heat->width-1;
				else if(target.x >= heat->width) target.x = 0;
				if(target.y == -1) target.y = heat->height-1;
				else if(target.y >= heat->height) target.y = 0;
				if(tries == 9) {
					//printf("cant move bug id: %d\n", id);
					target.x = x;
					target.y = y;
				}
			}
			heat->addHeat(outputHeat, x, y);
			x = target.x;
			y = target.y;
		}
	}

	template<class T>
	__device__ void step(HeatSpace<T>* heat, Grid2d<int>* world) {
		//if(id == 1) printf("step\n");
		settarget(heat);
		__syncthreads();
		move(heat, world);
	}
};