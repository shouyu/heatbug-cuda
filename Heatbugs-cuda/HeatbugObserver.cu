﻿
#include <sstream>
#include <iostream>
#include <cutil_inline.h>
#include "HeatbugModel.h"
#include "HeatbugObserver.h"
#include <ctime>

HeatbugObserver* HeatbugObserver::obs = 0;

void drawCallback() {
	HeatbugObserver::obs->display();
}

void keyboardCallback(unsigned char key, int x, int y) {
	HeatbugObserver::obs->keyfunc(key, x, y);
}

void mouseCallback(int button, int state, int x, int y) {
	HeatbugObserver::obs->mousefunc(button, state, x, y);
}

void motionCallback(int x, int y) {
	HeatbugObserver::obs->motionfunc(x, y);
}

void mouseWheelCallback(int wheel_number, int direction, int x, int y) {
	HeatbugObserver::obs->mousewheelfunc(wheel_number, direction, x, y);
}

void closeCallback() {
	HeatbugObserver::obs->closefunc();
}

HeatbugObserver::HeatbugObserver(int w, int h, int sw, int sh, int num)
	: width(w),
	  height(h),
	  vsync(0),
	  pause(false),
	  single(false),
	  modelWidth(sw),
	  modelHeight(sh),
	  modelCount(num),
	  tick(0),
	  fpsLimit(5),
	  fpsCount(0),
	  mouse_buttons(0),
	  translate_x(0),
	  translate_y(0),
	  scale(1.0)
{
	obs = this;
}

HeatbugObserver::~HeatbugObserver(void)
{
}

void HeatbugObserver::cleanup() {
	printf("called cleanup\n");
	cudaDeviceSynchronize();
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));

	//cutilSafeCall(
		cudaGraphicsUnregisterResource(cudaHeatRes);
	//	);
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	//cutilSafeCall(
		cudaGraphicsUnregisterResource(cudaBugRes);
	//	);
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glDeleteBuffers(1, &heatBuffer);
	glDeleteBuffers(1, &bugBuffer);

	cutilSafeCall(cudaFree((void**)heatbugModel));
	cudaDeviceSynchronize();
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));

	cutilSafeCall(cudaEventDestroy(startEvent));
    cutilSafeCall(cudaEventDestroy(stopEvent));
}

void HeatbugObserver::buildObjects() {
	int seed = 100;
	seed = (int)std::time(NULL);
	printf("seed: %d\n", seed);

	// Initialize GL
	int c=1;
	char* dummy = "";
	glutInit(&c, &dummy);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("Heatbug-cpp");
	glutKeyboardFunc(keyboardCallback);
	glutDisplayFunc(drawCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	glutMouseWheelFunc(mouseWheelCallback);
	glutCloseFunc(closeCallback);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	//glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);

	glewInit();

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, height, 0);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	// Initialize VBO
	glGenBuffers(1, &heatBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, heatBuffer);
	glBufferData(GL_ARRAY_BUFFER,
		modelWidth * modelHeight * 8 * 3 * sizeof(float),
		NULL, GL_DYNAMIC_DRAW);
	glGenBuffers(1, &bugBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bugBuffer);
	glBufferData(GL_ARRAY_BUFFER,
		modelCount * 8 * 3 * sizeof(float),
		NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	cudaGLSetGLDevice(cutGetMaxGflopsDeviceId());

	// Register VBO
	cutilSafeCall(cudaGraphicsGLRegisterBuffer(&cudaHeatRes, heatBuffer, cudaGraphicsMapFlagsNone));
	cutilSafeCall(cudaGraphicsGLRegisterBuffer(&cudaBugRes, bugBuffer, cudaGraphicsMapFlagsWriteDiscard));

	// Vsync off
	if(glewGetExtension("WGL_EXT_swap_control")) {
		wglSwapIntervalEXT(0);
	}
	
	// set device heap to 256MB
	cutilSafeCall(cudaThreadSetLimit(cudaLimitMallocHeapSize, 256 * 1024 * 1024));

	// Init Event
	cutilSafeCall( cudaEventCreate(&startEvent) );
    cutilSafeCall( cudaEventCreate(&stopEvent) );

	// Initialize HeatbugModel
	cudaMalloc(&heatbugModel, sizeof(HeatbugModel**));
	kernel::createHeatbugModel<<<1, 1>>>(heatbugModel, modelCount, modelWidth, modelHeight);
	kernel::buildObjects<<<1, 1>>>(heatbugModel);
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	cudaDeviceSynchronize();

	int gridx = calcGrids(modelWidth, blockSize);
    int gridy = calcGrids(modelHeight, blockSize);
	kernel::initSpaces<<<dim3(gridx, gridy), dim3(blockSize, blockSize)>>>(heatbugModel, seed);
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	cudaDeviceSynchronize();

	gridx = calcGrids(modelCount, blockSize);
	kernel::initModels<<<gridx, blockSize>>>(heatbugModel, seed);
	printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	cudaDeviceSynchronize();

	patchSize = (float)width / modelWidth;

	cutilSafeCall(cudaEventRecord(startEvent, 0));
	glutMainLoop();
}

int inline HeatbugObserver::calcGrids(int count, int block) {
	return ((count % block) != 0) ? (count / block + 1) : (count / block);
}

void HeatbugObserver::action() {
	int gridx = calcGrids(modelWidth, blockSize);
    int gridy = calcGrids(modelHeight, blockSize);

	kernel::diffuse<<<dim3(gridx, gridy), dim3(blockSize, blockSize)>>>(heatbugModel);
	kernel::swapBuffer<<<1, 1>>>(heatbugModel);

	gridx = calcGrids(modelCount, blockSize);
	kernel::stepbug<<<gridx, blockSize>>>(heatbugModel);

	//printf("tick:%d\n", tick);
	//kernel::printHeatSpace<<<1, 1>>>(heatbugModel);
	//cudaDeviceSynchronize();

	tick++;
}

void HeatbugObserver::nextFrame() {
	if(!pause || single) {
	this->action();

	// render heatspace
	cutilSafeCall(cudaGraphicsMapResources(1, &cudaHeatRes, 0));
	float3* heatArray;
	size_t numBytes;
	cutilSafeCall(cudaGraphicsResourceGetMappedPointer((void**)&heatArray, &numBytes, cudaHeatRes));
	int gridx = calcGrids(modelWidth, blockSize);
	int gridy = calcGrids(modelHeight, blockSize);
	kernel::renderHeatSpace<<<dim3(gridx, gridy), dim3(blockSize, blockSize)>>>(
		heatbugModel, heatArray, patchSize, numBytes / sizeof(float)
		);
	cutilSafeCall(cudaGraphicsUnmapResources(1, &cudaHeatRes, 0));
	//printf("%s¥n", cudaGetErrorString(cudaGetLastError()));

	// render bugs
	float3* bugArray;
	cutilSafeCall(cudaGraphicsMapResources(1, &cudaBugRes, 0));
	cutilSafeCall(
		cudaGraphicsResourceGetMappedPointer((void**)&bugArray, &numBytes, cudaBugRes)
	);
	gridx = calcGrids(modelCount, blockSize);
	kernel::renderBugs<<<gridx, blockSize>>>(
		heatbugModel, bugArray, patchSize, numBytes / sizeof(float)
		);
	cutilSafeCall(cudaGraphicsUnmapResources(1, &cudaBugRes, 0));
	//cudaThreadSynchronize();
	//printf("%s¥n", cudaGetErrorString(cudaGetLastError()));
	single = false;
	}
}

void HeatbugObserver::display() {
	nextFrame();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	glTranslatef(translate_x, translate_y, 0.0);
    glScalef(scale, scale, 1.0);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER, heatBuffer);
	glInterleavedArrays(GL_C3F_V3F, 0, NULL);
	glDrawArrays(GL_QUADS, 0, modelWidth * modelHeight * 4);

	glBindBuffer(GL_ARRAY_BUFFER, bugBuffer);
	glInterleavedArrays(GL_C3F_V3F, 0, NULL);
	glDrawArrays(GL_QUADS, 0, modelCount * 4);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

	glutSwapBuffers();
	glutPostRedisplay();

	getFPS();
}

void HeatbugObserver::keyfunc(unsigned char key, int x, int y) {
	// turn vsync
	if(key == 'v') {
		if(glewGetExtension("WGL_EXT_swap_control")) {
			vsync = 1 - vsync;
			if(vsync == 0) {
				wglSwapIntervalEXT(0);
			} else {
				wglSwapIntervalEXT(1);
			}
		}
	}
	// play/stop
	if(key == 's') {
		pause = !pause;
	}
	// single step
	if(key == 'd') {
		pause = true;
		single = true;
	}
	// reset
	if(key == 'r') {
		translate_x = 0;
		translate_y = 0;
		scale = 1.0;
	}
	if(key == 27) {
		glutLeaveMainLoop();
		cleanup();
		exit(0);
	}
}

void HeatbugObserver::mousefunc(int button, int state, int x, int y) {
	if (state == GLUT_DOWN) {
        mouse_buttons |= 1<<button;
    } else if (state == GLUT_UP) {
        mouse_buttons = 0;
    }

    mouse_old_x = x;
    mouse_old_y = y;
}

void HeatbugObserver::motionfunc(int x, int y) {
	float dx, dy;
    dx = (float)(x - mouse_old_x);
    dy = (float)(y - mouse_old_y);

    if (mouse_buttons & 1) {
		translate_x += dx * 0.8f;
		if  (translate_x < -width*(scale-1)) translate_x = -width*(scale-1);
		else if(translate_x >= 0) translate_x = 0;  
		translate_y += dy * 0.8f;
		if  (translate_y < -height*(scale-1)) translate_y = -height*(scale-1);
		else if(translate_y >= 0) translate_y = 0;  
    } else if (mouse_buttons & 4) {
		scale += dy * 0.01f;
		if(scale < 1) scale = 1.0;
		if  (translate_x < -width*(scale-1)) translate_x = -width*(scale-1);
		else if(translate_x >= 0) translate_x = 0;  
		if  (translate_y < -height*(scale-1)) translate_y = -height*(scale-1);
		else if(translate_y >= 0) translate_y = 0;  
    }

    mouse_old_x = x;
    mouse_old_y = y;
}

void HeatbugObserver::mousewheelfunc(int wheel_number, int direction, int x, int y) {
	if(direction==1) {
		scale += 0.3f;
	} else {
		scale -= 0.3f;
	}

	if(scale < 1) scale = 1.0;
	if  (translate_x < -width*(scale-1)) translate_x = -width*(scale-1);
	else if(translate_x >= 0) translate_x = 0;  
	if  (translate_y < -height*(scale-1)) translate_y = -height*(scale-1);
	else if(translate_y >= 0) translate_y = 0;  
}

void HeatbugObserver::closefunc() {
	cleanup();
}

void HeatbugObserver::getFPS() {
	static double ifps;

	fpsCount++;
    // this displays the frame rate updated every second (independent of frame rate)
    if (this->fpsCount >= this->fpsLimit) 
    {
        char fps[256];
    
        float milliseconds = 1;
        cutilSafeCall(cudaEventRecord(stopEvent, 0));  
        cutilSafeCall(cudaEventSynchronize(stopEvent));
        cutilSafeCall( cudaEventElapsedTime(&milliseconds, startEvent, stopEvent));
        
        milliseconds /= (float)fpsCount;

        ifps = 1.f / (milliseconds / 1000.f);
        sprintf(fps,
			    "CUDA Heatbugs(%dx%d, %d bugs): "
				"%0.1f fps | %d tick | VSYNC %d | Scale %.3f", 
				modelWidth, modelHeight, modelCount, ifps, tick, vsync, scale);  

        glutSetWindowTitle(fps);
        this->fpsCount = 0; 
        this->fpsLimit = (ifps > 1.f) ? (int)ifps : 1;
        if (pause) this->fpsLimit = 0;
        
        // restart timer
        cutilSafeCall(cudaEventRecord(startEvent, 0));
    }
}