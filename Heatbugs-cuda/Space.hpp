
#pragma once

template<class T>
struct Grid2d {
	__device__ Grid2d(int w, int h)
		: width(w), height(h)
	{
		data = new T[w*h];
	}

	__device__ virtual ~Grid2d() {
		delete[] data;
	}

	__device__ virtual void init(int id) {
		data[id] = 0;
	}

	__device__ T getValueAt(int x, int y) {
		return data[x + y * width];
	}

	T* data;
	int width;
	int height;
};

template<class T>
struct HeatSpace : public Grid2d<T>
{
	__device__ HeatSpace(int w, int h)
		: Grid2d<T>(w, h)
	{
		buffer = new T[w*h];
	}

	__device__ ~HeatSpace() {
		delete[] buffer;
	}

	__device__ void init(int id) {
		data[id] = 0;
	}

	__device__ void diffuse(int x, int y) {
		int xm1, xp1, ym1, yp1;
		T sum = 0;
		double delta, d;
		int offset = x + y * width;
		T newstate;

		xm1 = (x + width - 1) % width;
		xp1 = (x + 1) % width;
		ym1 = (y + height - 1) % height;
		yp1 = (y + 1) % height;

		sum += data[xm1 + ym1 * width];
		sum += data[x   + ym1 * width];
		sum += data[xp1 + ym1 * width];
		sum += data[xm1 + y   * width];
		sum += data[xp1 + y   * width];
		sum += data[xm1 + yp1 * width];
		sum += data[x   + yp1 * width];
		sum += data[xp1 + yp1 * width];
		delta = (double)sum / 8.0;

		d = (1 - diff) * data[offset] + diff * delta;
		d *= evap;
		newstate = (T)d;
		buffer[offset] = min(newstate, maxHeat);
	}

	__device__ void addHeat(int moreHeat, int x, int y) {
		int heatHere;

		heatHere = this->getValueAt(x, y);
		heatHere = min(maxHeat, heatHere+moreHeat);
		atomicExch(&data[x + y * width], heatHere);
	}

	__device__ void swapBuffer() {
		T* temp = data;
		data = buffer;
		buffer = temp;
	}

	double diff;
	double evap;
	static const int maxHeat = 0x7fff;
	T* buffer;
};