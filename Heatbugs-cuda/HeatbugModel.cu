#pragma once
#include <cutil_inline.h>
#include "HeatbugModel.h"
#include "Heatbug.hpp"
#include "Space.hpp"

struct cudaGraphicsResource;

__device__ HeatbugModel::HeatbugModel(int bugs, int width, int height)
	: numBugs(bugs),
	evaporationRate(0.99),
	diffuseConstant(1),
	width(width),
	height(height),
	minIdealTemp(17000),
	maxIdealTemp(31000),
	minOutputHeat(6000),
	maxOutputHeat(10000),
	randomMoveProbability(0.0) {}

__device__ HeatbugModel::~HeatbugModel(void) {}

__device__ void HeatbugModel::buildObjects() {
	printf("build objects\n");
	printf("width: %d, height: %d\n", width, height);
	printf("bugs: %d\n", numBugs);
	world = new Grid2d<int>(width, height);
	heat = new HeatSpace<int>(width, height);
	heatbugs = new Heatbug[numBugs];

	heat->diff = diffuseConstant;
	heat->evap = evaporationRate;
}

__device__ void HeatbugModel::initSpaces(int tid, int seed) {
	// Init HeatSpace
	heat->init(tid);

	// Init Grid2d
	world->init(tid);
}

__device__ void HeatbugModel::initModels(int tid, int seed) {
	heatbugs[tid].setId(tid+1);
	heatbugs[tid].initRNG(seed);
	heatbugs[tid].setOutputHeat(minOutputHeat, maxOutputHeat);
	heatbugs[tid].setIdealTemperature(minIdealTemp, maxIdealTemp);
	heatbugs[tid].setRandomMoveProbability(randomMoveProbability);
	heatbugs[tid].setRandomPos(width-1, height-1);
}

__device__ void HeatbugModel::diffuse(int x, int y) {
	heat->diffuse(x, y);
}

__device__ void HeatbugModel::swapBuffer() {
	heat->swapBuffer();
}

__device__ void HeatbugModel::stepbug(int tid) {
	heatbugs[tid].step(heat, world);
}

namespace kernel {

__global__ void createHeatbugModel(HeatbugModel** model, int bugs, int width, int height) {
	*model = new HeatbugModel(bugs, width, height);
}

__global__ void buildObjects(HeatbugModel** model) {
	(*model)->buildObjects();
}

__global__ void initSpaces(HeatbugModel** model, int seed) {
	int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

	if(x >= (*model)->width || y >= (*model)->height) return;
	(*model)->initSpaces(x + y * (*model)->width, seed);
}

__global__ void initModels(HeatbugModel** model, int seed) {
	int tid = blockDim.x * blockIdx.x + threadIdx.x;

	if(tid >= (*model)->numBugs) return;
	(*model)->initModels(tid, seed);
}

__global__ void diffuse(HeatbugModel** model) {
	int x = blockDim.x * blockIdx.x + threadIdx.x;
    int y = blockDim.y * blockIdx.y + threadIdx.y;

	if(x >= (*model)->width || y >= (*model)->height) return;
    (*model)->diffuse(x, y);
}

__global__ void swapBuffer(HeatbugModel** model) {
	(*model)->swapBuffer();
}

__global__ void stepbug(HeatbugModel** model) {
	int tid = blockDim.x * blockIdx.x + threadIdx.x;
	if(tid >= (*model)->numBugs) return;
	(*model)->stepbug(tid);
}

__global__ void renderHeatSpace(HeatbugModel** model, float3* heatArray, float patch, size_t numBytes) {
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
	//unsigned int tid = x + y * blockDim.x * gridDim.x;
	unsigned int tid = x + y * (*model)->width;

	if(x >= (*model)->width || y >= (*model)->height) return;

	float color = (float)(*model)->heat->data[tid] / (*model)->heat->maxHeat;
	heatArray[8 * tid    ] = make_float3(color, 0, 0);
	heatArray[8 * tid + 1] = make_float3(x * patch, y * patch , 0);
	heatArray[8 * tid + 2] = make_float3(color, 0, 0);
	heatArray[8 * tid + 3] = make_float3(x * patch + patch, y * patch , 0);
	heatArray[8 * tid + 4] = make_float3(color, 0, 0);
	heatArray[8 * tid + 5] = make_float3(x * patch + patch, y * patch + patch , 0);
	heatArray[8 * tid + 6] = make_float3(color, 0, 0);
	heatArray[8 * tid + 7] = make_float3(x * patch, y * patch + patch , 0);
}

__global__ void renderBugs(HeatbugModel** model, float3* bugArray, float patch, size_t numBytes) {
	unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= (*model)->numBugs) return;
	int x = (*model)->heatbugs[tid].x;
	int y = (*model)->heatbugs[tid].y;
	bugArray[8 * tid    ] = make_float3(1.0f, 1.0f, 1.0f);
	bugArray[8 * tid + 1] =	make_float3(x * patch + patch / 2, y * patch, 0);
	bugArray[8 * tid + 2] = make_float3(1.0f, 1.0f, 1.0f);
	bugArray[8 * tid + 3] = make_float3(x * patch, y * patch + patch / 2, 0);
	bugArray[8 * tid + 4] = make_float3(1.0f, 1.0f, 1.0f);
	bugArray[8 * tid + 5] = make_float3(x * patch + patch / 2, y * patch + patch, 0);
	bugArray[8 * tid + 6] = make_float3(1.0f, 1.0f, 1.0f);
	bugArray[8 * tid + 7] = make_float3(x * patch + patch, y * patch + patch / 2, 0);
}

__global__ void printHeatSpace(HeatbugModel** model) {
	printf("width:%d, height:%d\n", (*model)->width, (*model)->height);
	for(int x = 0; x < (*model)->width; x++) {
		for(int y = 0; y < (*model)->height; y++) {
			printf("%d ", (*model)->world->getValueAt(x, y));
		}
		printf("\n");
	}
	printf("\n");
}

} // end namespace kernel