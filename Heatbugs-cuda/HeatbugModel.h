#pragma once
#include <cutil_inline.h>

class Heatbug;
template<class T> struct Grid2d;
template<class T> struct HeatSpace;

class HeatbugModel
{
public:
	__device__ HeatbugModel(int bugs, int width, int height);
	__device__ ~HeatbugModel(void);
	__device__ void buildObjects();
	__device__ void initSpaces(int tid, int seed);
	__device__ void initModels(int tid, int seed);
	__device__ void diffuse(int x, int y);
	__device__ void swapBuffer();
	__device__ void stepbug(int tid);

	int numBugs;
	double evaporationRate;
	double diffuseConstant;
	int width, height;
	int minIdealTemp, maxIdealTemp;
	int minOutputHeat, maxOutputHeat;
	double randomMoveProbability;
	volatile int globalsync;

	Heatbug* heatbugs;
	Grid2d<int>* world;
	HeatSpace<int>* heat;
};

namespace kernel {

__global__ void createHeatbugModel(HeatbugModel** model, int bugs, int width, int height);
__global__ void buildObjects(HeatbugModel** model);
__global__ void initSpaces(HeatbugModel** model, int seed);
__global__ void initModels(HeatbugModel** model, int seed);
__global__ void diffuse(HeatbugModel** model);
__global__ void swapBuffer(HeatbugModel** model);
__global__ void stepbug(HeatbugModel** model);
__global__ void renderHeatSpace(HeatbugModel** model, float3* heatArray, float patch, size_t numBytes);
__global__ void renderBugs(HeatbugModel** model, float3* bugArray, float patch, size_t numBytes);
__global__ void printHeatSpace(HeatbugModel** model);

} // end namespace kernel
