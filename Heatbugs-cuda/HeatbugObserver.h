
#pragma once
#pragma comment(lib, "glew32.lib")
#include "GLHelper.h"
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/freeglut.h>
#include <cutil_inline.h>    // includes cuda.h and cuda_runtime_api.h
#include <cutil_gl_inline.h> // includes cuda_gl_interop.h// includes cuda_gl_interop.h

class HeatbugModel;

class HeatbugObserver
{
public:
	HeatbugObserver(int w, int h, int sw, int sh, int num);
	~HeatbugObserver(void);

	HeatbugModel **heatbugModel;
	static HeatbugObserver* obs;
	void buildObjects();
	void action();
	void display();
	void keyfunc(unsigned char key, int x, int y);
	int inline calcGrids(int count, int block);
	void mousefunc(int button, int state, int x, int y);
	void motionfunc(int x, int y);
	void mousewheelfunc(int wheel_number, int direction, int x, int y);
	void closefunc();
	void cleanup();
private:
	void getFPS();
	void nextFrame();
	int width;
	int height;
	float patchSize;
	GLuint heatBuffer;
	GLuint bugBuffer;
	cudaGraphicsResource *cudaHeatRes;
	cudaGraphicsResource *cudaBugRes;
	int vsync;
	bool pause;
	bool single;
	unsigned int tick;

	int modelWidth;
	int modelHeight;
	int modelCount;
	static const int blockSize = 16;
	int fpsLimit;
	int fpsCount;
	cudaEvent_t startEvent, stopEvent;
	int mouse_buttons;
	int mouse_old_x, mouse_old_y;
	float translate_x, translate_y, scale;
};

extern "C" {
	void drawCallback();
	void keyboardCallback(unsigned char key, int x, int y);
	void mouseCallback(int button, int state, int x, int y);
	void motionCallback(int x, int y);
	void closeCallback();
}